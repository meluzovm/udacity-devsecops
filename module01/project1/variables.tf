variable "aws_profile" {
  description = "AWS profile which should be used in the provider"
  default     = "123456789012"
}

variable "aws_account" {
  description = "AWS account to create the bucket in"
  default     = "123456789012"
}

