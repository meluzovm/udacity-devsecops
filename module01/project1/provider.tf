# Provider in the region Frankfurt

provider "aws" {
  profile = var.aws_profile
  region  = "eu-central-1"
}

